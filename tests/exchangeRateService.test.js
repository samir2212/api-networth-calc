const nock = require('nock');
const exchangeRateService = require('../api/exchangeRateService');

describe('Test exchange rates service', () => {
  afterEach(() => {
    nock.cleanAll();
  });
  it('should return the rates when backend call succeeds', async () => {
    const baseCurrency = 'CAD';
    nock(`${exchangeRateService.EXCHANGE_RATE_URI}`)
      .persist()
      .get(/.*/)
      .reply(200, {
        rates: {
          CAD:1.0,
          HKD:5.8668219763,
          ISK:105.6105610561,
          PHP:36.9222804633,
          DKK:4.8164757652,
          HUF:235.9282987122,
          CZK:17.692357471,
        },
        base: 'CAD',
        date: '2020-10-15',
     });
    const rates = await exchangeRateService.getExchangeRates(baseCurrency);
    expect(rates).not.toBeNull();
    expect(rates.base).toEqual(baseCurrency);
  });

  it('should return the error when backend call fails', async () => {
    const baseCurrency = 'CAD';
    nock(`${exchangeRateService.EXCHANGE_RATE_URI}`)
      .persist()
      .get(/.*/)
      .replyWithError(500, {});
    const rates = await exchangeRateService.getExchangeRates(baseCurrency);
    expect(rates).toMatchObject(new Error('Unable to retrieve rates'));
  });
});