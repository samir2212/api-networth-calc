const controller = require('../api/netWorthCalculatorController');
const exchangeRatesService = require('../api/exchangeRateService'); 

describe('Test net worth calculator controller', () => {
  let req;
  let res;
  let send;
  beforeEach(() => {
    req = { body: {
      assets: {cashInvestments: 10}, currency: 'USD', fromCurrency: 'CAD'
    }, query: { isConversionRequired: true }};
    send = jest.fn();
    res = {
      status: jest.fn().mockImplementation(() => ({ send })),
      send,
    };
    exchangeRatesService.getExchangeRates = jest.fn().mockImplementation(() => {
      return Promise.resolve({
        rates: {
          CAD:1.0,
          HKD:5.8668219763,
          ISK:105.6105610561,
          PHP:36.9222804633,
          DKK:4.8164757652,
          HUF:235.9282987122,
          USD:0.7576305091,
        }
      });
    });
  });
  afterEach((done) => {
    jest.clearAllMocks();
    done();
  });
  it('Should perform currency conversion when isConversionRequired is true', async (done) => {
    await controller.calculate(req, res);
    expect(send).toHaveBeenCalledWith({
      assets: {
        cashInvestments: 7.6
      },
      liabilities: {},
      netWorth: 7.6,
      totalAssets: 7.6,
      totalLiabilities: 0
    });
    done();
  });

  it('Should not perform currency conversion when isConversionRequired is false', async (done) => {
    let request = { body: {
      assets: {cashInvestments: 10}, currency: 'USD', fromCurrency: 'CAD'
    }, query: { isConversionRequired: false }};
    await controller.calculate(request, res);
    expect(send).toHaveBeenCalledWith({
      assets: {
        cashInvestments: 10
      },
      liabilities: {},
      netWorth: 10,
      totalAssets: 10,
      totalLiabilities: 0
    });
    done();
  });

  it('Should calculate networth', async (done) => {
    let req = { body: {
      assets: {cashInvestments: 10}, liabilities: { mortgage1: 5}, currency: 'USD', fromCurrency: 'CAD'
    }, query: { isConversionRequired: false }};
    const send = jest.fn();
    let res = {
      status: jest.fn().mockImplementation(() => ({ send })),
      send,
    };
    await controller.calculate(req, res);
    expect(send).toHaveBeenCalledWith({
      assets: {
        cashInvestments: 10
      },
      liabilities: {
        mortgage1: 5,
      },
      netWorth: 5,
      totalAssets: 10,
      totalLiabilities: 5
    });
    done();
  });
});