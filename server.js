let express = require('express'),
  app = express(),
  port = process.env.PORT || 7890,
  bodyParser = require('body-parser');

let cors = require('cors');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200
}));

var routes = require('./api/routes');
routes(app);

app.listen(port);

module.exports = app;