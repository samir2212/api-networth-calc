const axios = require('axios');
const adapter = require('axios/lib/adapters/http');

const EXCHANGE_RATE_URI = 'https://api.exchangeratesapi.io/latest';
const Api = axios.create({
  baseURL: EXCHANGE_RATE_URI,
});
const getExchangeRates = async (baseCurrency) => {
  try{
    const response = await Api.get(`?base=${baseCurrency}`, {adapter});
    return response.data;
  } catch (e) {
    return new Error('Unable to retrieve rates');
  }

}
module.exports = {
  getExchangeRates,
  EXCHANGE_RATE_URI,
};
