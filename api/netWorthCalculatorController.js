'use strict';

const exchangeService = require('./exchangeRateService');

Number.prototype.toFixedNumber = function (digits, base) {
  var pow = Math.pow(base||10, digits);
  return Math.round(this*pow) / pow;
}

exports.calculate = async (req, res) => {
  const { assets, liabilities, currency, fromCurrency } = req.body;
  const isConversionRequired = req.query.isConversionRequired;

  let currencyMultiplier = 1;
  if (isConversionRequired) {
    const exchangeRates = await exchangeService.getExchangeRates(fromCurrency);
    currencyMultiplier = exchangeRates.rates[currency];
  }

  const responseAssets = {};
  let totalAssets = 0;
  if (assets) {
    const assetKeys = Object.keys(assets);
    assetKeys.forEach(asset => {
      responseAssets[asset] = (assets[asset] * currencyMultiplier).toFixedNumber(2);
      totalAssets += (assets[asset] * currencyMultiplier).toFixedNumber(2);
    });
  }

  const responseLiabilities = {};
  let totalLiabilities = 0;
  if (liabilities) {
    const liabilitiesKeys = Object.keys(liabilities);
    liabilitiesKeys.forEach(liability => {
      responseLiabilities[liability] = (liabilities[liability] * currencyMultiplier).toFixedNumber(2);
      totalLiabilities += (liabilities[liability] * currencyMultiplier).toFixedNumber(2);
    });
  }
  return res.status(200).send({
    liabilities: responseLiabilities,
    assets: responseAssets,
    totalAssets,
    totalLiabilities,
    netWorth: totalAssets - totalLiabilities
  });
};