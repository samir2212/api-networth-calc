'use strict';
module.exports = function(app) {
  const calculator = require('./netWorthCalculatorController');
  app.route('/calculate')
    .post(calculator.calculate);
};